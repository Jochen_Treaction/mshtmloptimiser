<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class CheckAliveController extends AbstractController
{

    /**
     * @Route( "/check/alive", name="checkAlive", methods={"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function checkAlive(Request $request):JsonResponse
    {
        return $this->json([
            'success' => true,
            'msg' => 'i am alive',
            'yourIp' => $request->getClientIp(),
            'locale' => $request->getLocale(),
            'protocolVersion' => $request->getProtocolVersion(),
        ]);
    }

}
