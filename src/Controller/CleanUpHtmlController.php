<?php

namespace App\Controller;

use App\Services\ToolServices;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Services\CleanHtmlService;
use Psr\Log\LoggerInterface;


/**
 * @Route( "/advertorial")
 * provides temporary route to Clean Up Html - Minimum Viable Product
 * accessible only via direct route call - not provided in menu
 */
class CleanUpHtmlController extends AbstractController
{
    /**
     * @Route( "/cleanup/html", name="cleanUpHtml", methods={"POST"})
     * @param Request          $request   content-type: application/json,  e.g. { <b>html</b> : <i>some base64-encoded html file content</i>, <b>replaceTelNoAnkers</b> : <i>bool</i>}
     * @param CleanHtmlService $cleanHtmlService
     * @param LoggerInterface  $logger
     * @return JsonResponse
     */
    public function cleanUpHtml(Request $request,CleanHtmlService $cleanHtmlService, LoggerInterface $logger, ToolServices $toolServices):Response
    {
        $logger->info('RUNNING cleanUpHtml WITHOUT inlineCss', [__METHOD__,__LINE__]);
        $response = new Response();
        $response = $toolServices->setHeadersToAvoidCORSBlocking($response);

        $json = $request->getContent();

        try {
            $jsonDecode = json_decode($json, true, 10, JSON_OBJECT_AS_ARRAY);
            $logger->info('$jsonDecode', [$jsonDecode,__METHOD__,__LINE__]);
        } catch (\Exception $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $toolServices->setJsonResponse($response, ['success' => false, 'msg' => $e->getMessage(), 'html'=>'']);
        }

        $html = base64_decode($jsonDecode['html']);
        // $replaceTelNoAnkers = $jsonDecode['replaceTelNoAnkers'];
        $cbxSettings = $jsonDecode['cbxSettings'];
        $cleanhtml = $cleanHtmlService->cleanUpHtml($html, $cbxSettings);

        return $toolServices->setJsonResponse($response, ['success' => true, 'msg' => 'done', 'cleanhtml' => base64_encode($cleanhtml)]);
    }


    /**
     * @deprecated since 2022-03-28 - <b>no longer referenced as single route. Use route <code>/inlinecss/cleanup/html</code> instead</b>
     * @Route( "/inlinecss/html", name="inlineCss", methods={"POST"})
     * @param Request          $request  content-type : text/plain,  base64-encoded( pure html content)
     * @param CleanHtmlService $cleanHtmlService
     * @return JsonResponse
     */
    public function inlineCss(Request $request, CleanHtmlService $cleanHtmlService, ToolServices $toolServices):Response
    {
        $response = new Response();
        $response = $toolServices->setHeadersToAvoidCORSBlocking($response);

        $html = base64_decode($request->getContent());
        $inlineCssHtml = $cleanHtmlService->inlineCss($html);
        return $toolServices->setJsonResponse($response, ['success' => true, 'msg' => 'done', 'cleanhtml' => base64_encode($inlineCssHtml)]);
    }


    /**
     * @Route( "/inlinecss/cleanup/html", name="inlineCssCleanUpHtml", methods={"POST"})
     * @param Request          $request content-type: application/json,  e.g. { <b>html</b> : <i>some base64-encoded html file content</i>, <b>replaceTelNoAnkers</b> : <i>bool</i>}
     * @param CleanHtmlService $cleanHtmlService
     * @param LoggerInterface  $logger
     * @return JsonResponse
     */
    public function inlineCssAndCleanupHtml(Request $request,CleanHtmlService $cleanHtmlService, LoggerInterface $logger, ToolServices $toolServices):Response
    {
        $logger->info('RUNNING inlineCssCleanUpHtml WITH inlineCss', [__METHOD__,__LINE__]);
        $json = $request->getContent();
        $response = new Response();
        $response = $toolServices->setHeadersToAvoidCORSBlocking($response);

        try {
            $jsonDecode = json_decode($json, true, 10, JSON_OBJECT_AS_ARRAY);
            $logger->info('$jsonDecode', [$jsonDecode,__METHOD__,__LINE__]);
        } catch (\Exception $e) {
            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
            return $response->setContent(json_encode(['success' => false, 'msg' => $e->getMessage(), 'cleanhtml'=>'']));
        }

        $html = base64_decode($jsonDecode['html']);
        // $replaceTelNoAnkers = $jsonDecode['replaceTelNoAnkers'];
        $cbxSettings = $jsonDecode['cbxSettings'];

        $inlineCssHtml = $cleanHtmlService->inlineCss($html);
        $cleanhtml = $cleanHtmlService->cleanUpHtml($inlineCssHtml, $cbxSettings);

        if( $_ENV['APP_ENV'] === 'dev') {
            file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'../../var/log/html_controller.hmtl', base64_decode(base64_encode($cleanhtml)));
            file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'../../var/log/html_base64.hmtl', base64_encode($cleanhtml));
        }

        return $toolServices->setJsonResponse($response, ['success' => true, 'msg' => 'done', 'cleanhtml' => base64_encode($cleanhtml), 'inlineCssHtml' => base64_encode($inlineCssHtml)]);
    }


//    /**
//     * @Route( "/test", name="test", methods={"POST"})
//     * @param Request          $request content-type: application/json,  e.g. { <b>html</b> : <i>some base64-encoded html file content</i>, <b>replaceTelNoAnkers</b> : <i>bool</i>}
//     * @param CleanHtmlService $cleanHtmlService
//     * @param LoggerInterface  $logger
//     * @return JsonResponse
//     */
//    public function test(Request $request,CleanHtmlService $cleanHtmlService, LoggerInterface $logger, ToolServices $toolServices):Response
//    {
//        $json = $request->getContent();
//        $response = new Response();
//        $response = $toolServices->setHeadersToAvoidCORSBlocking($response);
//
//        try {
//            $jsonDecode = json_decode($json, true, 10, JSON_OBJECT_AS_ARRAY);
//            $logger->info('$jsonDecode', [$jsonDecode,__METHOD__,__LINE__]);
//        } catch (\Exception $e) {
//            $logger->error($e->getMessage(), [__METHOD__, __LINE__]);
//            return $response->setContent(json_encode(['success' => false, 'msg' => $e->getMessage(), 'cleanhtml'=>'']));
//        }
//
//        $html = base64_decode($jsonDecode['html']);
//        // $replaceTelNoAnkers = $jsonDecode['replaceTelNoAnkers'];
//
//
//        $inlineCssHtml = $cleanHtmlService->inlineCss($html);
//        $cleanhtml = $cleanHtmlService->cleanUpHtmlParseDom($inlineCssHtml);
//
//        if( $_ENV['APP_ENV'] === 'dev') {
//            file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'../../var/log/html_controller.hmtl', base64_decode(base64_encode($cleanhtml)));
//            file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'../../var/log/html_base64.hmtl', base64_encode($cleanhtml));
//        }
//
//        return $toolServices->setJsonResponse($response, ['success' => true, 'msg' => 'done', 'cleanhtml' => base64_encode($cleanhtml), 'inlineCssHtml' => base64_encode($inlineCssHtml)]);
//    }
}
