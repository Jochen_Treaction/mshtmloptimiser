<?php

namespace App\Services;

class SecurityService
{
    /**
     * @internal must be call by each and every route before proceeding
     * @param string $ip
     * @return bool
     */
    public function isAllowedIp(string $ip): bool
    {
        // defines all ipv4 / ipv6, which are allowed to call routes of this Microservice

        return in_array($ip, [
            '127.0.0.1', // localhost TODO: REMOVE LATER
            '62.138.184.191', // CM new on centOS 63.32.105.231
            '2001:1520:1:200::3d6', // CM new on centOS -> this ipv6 arrives fom CM !!
            '185.158.183.223', // office ip
            '91.250.80.210', // marketing-in-one production
            '2a01:488:67:1000:5bfa:50d2:0:1', // marketing-in-one production
            '5.35.251.84', // api-in-one production
            '2a01:488:67:1000:523:fb54:0:1', // api-in-one production
            '92.51.150.31', // dev-campaign-in-one.net, dev-api-in-one.net
            '62.138.185.206', // test-marketing-in-one.net, test-api-in-one.net
            '172.31.13.27', // aws-api-in-one production private ip
            '172.31.36.31', // aws-marketing-in-one production private ip
            '18.196.32.177', // aws-api-in-one production public ip
            '18.196.60.233', // aws-marketing-in-one production public ip
        ]);
    }
}
