<?php


namespace App\Services;


use Exception;
use JsonException;
use RuntimeException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * taken from AIO
 */
class ToolServices
{

    /**
     * @param Response $response
     * @return Response
     * @author Pradeep
     */
    public function setHeadersToAvoidCORSBlocking(Response $response): Response
    {
        $response->headers->set('Content-Type', 'application/json');
        //$response->headers->set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
        //$response->headers->set('Content-Type', 'text/plain');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Headers',
            'access-control-allow-headers,access-control-allow-methods,access-control-allow-origin,access-control-max-age,content-type,bearer');
        $response->headers->set('Access-Control-Max-Age', '86400');
        $response->headers->set('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE');
        return $response;
    }

    /**
     * @param Response $response
     * @param string $message
     * @param bool $status
     * @param array|null $response_body
     * @return Response
     * @author Pradeep
     */
    public function setResponseWithMessage(Response $response, string $message, bool $status, array $response_body=null): Response
    {
        $return_response = $response;
        try {
            $return_response = $response->setContent(json_encode([
                'status' => $status,
                'message' => $message,
                'response' => $response_body,
            ], JSON_THROW_ON_ERROR));
        } catch (JsonException  | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $return_response;
    }

    /**
     * @param Response $response
     * @param array $response_body
     * @param bool $status
     * @param string|null $message
     * @return Response
     * @author Pradeep
     */
    public function setJsonResponse(
        Response $response,
        array $response_body,
        bool $status = false,
        string $message = null
    ): Response {
        $return = $response;
        try {
            $return = $response->setContent(json_encode([
                'status' => $status,
                'message' => $message,
                'response' => $response_body,
            ], JSON_THROW_ON_ERROR));
        } catch (JsonException | Exception $e) {
            $this->logger->error($e->getMessage(), [__METHOD__, __LINE__]);
        }
        return $return;
    }
}
