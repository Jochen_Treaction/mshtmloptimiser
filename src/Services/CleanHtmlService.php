<?php

namespace App\Services;

ini_set('memory_limit', "1000000000");

use Psr\Log\LoggerInterface;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;
use DOMDocument;

class CleanHtmlService
{
    private const EMPTY_STRING = 'empty';
    private const BLANK_STRING = 'blank';

    private LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }


    /**
     * @param string $html
     * @param bool   $replaceTelNoAnkers
     * @return string cleaned html code
     */
    public function cleanUpHtml(string $html, array $cbxSettings): string
    {
        $this->logger->info('cleanUpHtml $html', [$html, __METHOD__, __LINE__]);
        $this->logger->info('$cbxSettings', [$cbxSettings, __METHOD__, __LINE__]);
        $this->logger->info('$cbxSettings runInlineCss', [$cbxSettings['runInlineCss'], __METHOD__, __LINE__]);

        if( $_ENV['APP_ENV'] === 'dev') {
            file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'../../var/log/html_in.hmtl', $html);
        }
        // V6.1 + remove open pixel / tracking pixel + allow non inline css
        $removeOpenPixel = ($cbxSettings['removeOpenPixel']) ? true : false; // runs DOM parser and removes tracking and open pixel img elements
        $regExpParams = $this->setRegExprParams($cbxSettings);

        $this->logger->info('$cbxSettings', [$cbxSettings, __METHOD__, __LINE__]);
        $this->logger->info('$regExpParams', [$regExpParams, __METHOD__, __LINE__]);

        $cleanhtml = preg_replace(
            $regExpParams['pattern'],
            $regExpParams['replace'],
            $html
        );

        if($_ENV['APP_ENV'] === 'dev') {
            try {
                file_put_contents(__DIR__ . '/../../var/log/preg_replace.json', json_encode(array_combine($regExpParams['pattern'], $regExpParams['replace']), JSON_PRETTY_PRINT));
            } catch (\Exception $e) {
                $this->logger->error('file_put_contents failed', [$e->getMessage(), __METHOD__, __LINE__]);
                file_put_contents(__DIR__ . '/../../var/log/preg_replace_pattern_replace.json', json_encode(['pattern' => $regExpParams['pattern'], 'replace' => $regExpParams['replace']], JSON_PRETTY_PRINT));
            }
        }

        if($removeOpenPixel) {
            $cleanhtml = $this->removeOpenPixelImages($cleanhtml);
        }

        if( $_ENV['APP_ENV'] === 'dev') {
            file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'../../var/log/html_out.hmtl', $cleanhtml);
        }

        return $cleanhtml;
    }


    /**
     * set preg_replace array parameters depending on user defined settings (de/activated checkboxes in advertorial wizard UI)<br>
     * <b>CAUTION: DO NOT CHANGE THE ORDER OF SETTINGS - TO ADD NEW REPLACEMENTS CHECK CAREFULLY AT WHICH ARRAY INDEX POSITION TO ADD THEM</b>
     * @param array $cbxSettings
     * @return array [ pattern[], replace[] ] an array with search patterns and corresponding (index identical) replacement strings
     */
    private function setRegExprParams(array $cbxSettings):array
    {
        $this->logger->info('running '.__FUNCTION__, [__METHOD__, __LINE__]);
        // url is currently not used
        // $url = '/(?:(?:(?:https?):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:[/?#]\S*)?/i';

        // $title = ($cbxSettings['removeTitle']) ? '/<title.*?<\/title>/mi' : '//';
        $pattern[] = ($cbxSettings['removeTitle']) ? '/<title.*?<\/title>/mi' : null;
        $replace[] = ($cbxSettings['removeTitle']) ? self::EMPTY_STRING : null;

        // $link_rel_icon = ($cbxSettings['removeHeaderImages']) ? '/<link.*rel=[\'"]{1}icon[\'"]{1}.*>/' : '//';
        $pattern[] = ($cbxSettings['removeHeaderImages']) ? '/<link.*rel=[\'"]{1}icon[\'"]{1}.*>/' : null;
        $replace[] = ($cbxSettings['removeHeaderImages']) ? self::EMPTY_STRING : null;

        // $href_tel_singleline = ($cbxSettings['removePhoneUrl']) ? '/<a.*(href=[\'"]{1}((tel|mobil|handy|fon|telefon|festnetz|phone)?[\+ :\-\/0-9]+)[\'"]{1}).*>.*?<\/a>/mi' : '//';
        $pattern[] = ($cbxSettings['removePhoneUrl']) ? '/<a.*(href=[\'"]{1}((tel|mobil|handy|fon|telefon|festnetz|phone)?[\+ :\-\/0-9]+)[\'"]{1}).*>.*?<\/a>/mi' : null;
        $replace[] = ($cbxSettings['removePhoneUrl']) ? '<div>$2</div>' : null;

        // $comments = ($cbxSettings['removeComments']) ? "/<!--.*?-->/mis" : '//';
        $pattern[] = ($cbxSettings['removeComments']) ? '/<!--.*?-->/mis' : null;
        $replace[] = ($cbxSettings['removeComments']) ? self::EMPTY_STRING : null;

        // $scripts = ($cbxSettings['removeScripts']) ? '/<script.*?<\/script>/mis' : '//';
        $pattern[] = ($cbxSettings['removeScripts']) ? '/<script.*?<\/script>/mis' : null;
        $replace[] = ($cbxSettings['removeScripts']) ? self::EMPTY_STRING : null;

        // $classes = ($cbxSettings['runInlineCss']) ? '/class\s*=\s*"[a-z0-9_ \-]*"/i' : '//';
        $pattern[] = ($cbxSettings['runInlineCss']) ? '/class\s*=\s*"[a-z0-9_ \-]*"/i' : null;
        $replace[] = ($cbxSettings['runInlineCss']) ? self::BLANK_STRING : null; // blank

        // $microsoft_mso = ($cbxSettings['runInlineCss']) ? '/mso\-[a-z0-9\-_]*\s*:\s*[0-9a-z\- _%!#]*;?/i' : '//'; //  mso- in style
        $pattern[] = ($cbxSettings['runInlineCss']) ? '/mso\-[a-z0-9\-_]*\s*:\s*[0-9a-z\- _%!#]*;?/i' : null;
        $replace[] = ($cbxSettings['runInlineCss']) ? ';' : null;

        // $microsoft_ms = ($cbxSettings['runInlineCss']) ? '/\-ms\-[a-z0-9\-_]*\s*:\s*[0-9a-z\- _%!#]*;?/i' : '//'; // -ms in style
        $pattern[] = ($cbxSettings['runInlineCss']) ? '/\-ms\-[a-z0-9\-_]*\s*:\s*[0-9a-z\- _%!#]*;?/i' : null;
        $replace[] = ($cbxSettings['runInlineCss']) ? ';' : null;

        // $style = ($cbxSettings['runInlineCss']) ? '/style=";>/' : '//';
        $pattern[] = ($cbxSettings['runInlineCss']) ? '/style=";>/' : null;
        $replace[] = ($cbxSettings['runInlineCss']) ? '>' : null;

        // $blank_semicolon = ($cbxSettings['runInlineCss']) ? '/ ;/' : '//';
        $pattern[] = ($cbxSettings['runInlineCss']) ? '/ ;/' : null;
        $replace[] = ($cbxSettings['runInlineCss']) ? ';' : null;

        // $duplicate_semicolons = ($cbxSettings['runInlineCss']) ? "/;{2,}/" : '//';
        $pattern[] = ($cbxSettings['runInlineCss']) ? '/;{2,}/' : null;
        $replace[] = ($cbxSettings['runInlineCss']) ? ';' : null;

        // $spacelines = ($cbxSettings['removeEmptyLines']) ? '/^\s+$/ims' : '//';
        $pattern[] = ($cbxSettings['removeEmptyLines']) ? '/^\s+$/ims' : null;
        $replace[] = ($cbxSettings['removeEmptyLines']) ? self::EMPTY_STRING : null;

        // $allStyleTags = ($cbxSettings['runInlineCss']) ? '/<style.*?<\/style>/mis' : '//';
        $pattern[] = ($cbxSettings['runInlineCss']) ? '/<style.*?<\/style>/mis' : null;
        $replace[] = ($cbxSettings['runInlineCss']) ? self::EMPTY_STRING : null;

        // $duplicate_crlf = ($cbxSettings['removeEmptyLines']) ? '/\n{2,}/ims' : '//';
        $pattern[] = ($cbxSettings['removeEmptyLines']) ? '/\n{2,}/ims' : null;
        $replace[] = ($cbxSettings['removeEmptyLines']) ? "\n" : null;

        // $nonBreakingSpace = "/&nbsp;/"; - always removed
        $pattern[] =  '/&nbsp;/';
        $replace[] = ' ';

        // $ankerTextDomains = '/(>((www)?\.?(\w{1,})\.([a-z]{1,12})\.?([a-z]{0,})))<\/a>/i'; - always removed
        $pattern[] = '/(>((www)?\.?(\w{1,})\.([a-z]{1,12})\.?([a-z]{0,})))<\/a>/i';
        $replace[] = '<span>$4 (dot) $5 $6</span>';

        //  $head = ($cbxSettings['replaceHead']) ? '/.*<\/head>/ms' :'//';
        $pattern[] = ($cbxSettings['replaceHead']) ? '/.*<\/head>/ms' : null;
        $replace[] = ($cbxSettings['replaceHead']) ? '<!DOCTYPE html><html><head><meta content="text/html; charset=utf-8" http-equiv="content-type"><meta content="text/html; charset=utf-8" http-equiv="content-type" style="box-sizing: border-box;"></head>' :null;

        // $lang=($cbxSettings['removeAttributes']) ? '/lang="[a-z]{1,4}"/i' : '//';
        $pattern[] = ($cbxSettings['removeAttributes']) ? '/lang="[a-z]{1,4}"/i' : null;
        $replace[] = ($cbxSettings['removeAttributes']) ? self::EMPTY_STRING : null;

        // $role=($cbxSettings['removeAttributes']) ?'/role="[a-z0-9_\- ]+"/i' : '//';
        $pattern[] = ($cbxSettings['removeAttributes']) ? '/role="[a-z0-9_\- ]+"/i' : null;
        $replace[] = ($cbxSettings['removeAttributes']) ? self::EMPTY_STRING : null;

        // $aria=($cbxSettings['removeAttributes']) ? '/aria[\-]{0,1}[a-z]+="[\w\s]+"|aria="[\w\s]+"/i' : '//'; // keine Berücksichtigung von Sonderzeichen => No recognition of special characters: aria will stay!
        $pattern[] = ($cbxSettings['removeAttributes']) ? '/aria[\-]{0,1}[a-z]+="[\w\s]+"|aria="[\w\s]+"/i' : null;
        $replace[] = ($cbxSettings['removeAttributes']) ? self::EMPTY_STRING : null;

        // $betweenEndHeadStartBody=($cbxSettings['removeBetweenHeadBody']) ?'/<\/head>.*<body/is' : '//';
        $pattern[] = ($cbxSettings['removeBetweenHeadBody']) ? '/<\/head>.*<body/is' : null;
        $replace[] = ($cbxSettings['removeBetweenHeadBody']) ? '</head><body' : null;

        // remove all empty array records with values (null, '', false)
        $pattern = array_filter($pattern);
        $replace = array_filter($replace);
        // replace 'empty' by ''
        foreach ($replace as &$value) {
            if(self::EMPTY_STRING === $value) {
                $value = '';
            }
            if(self::BLANK_STRING === $value) {
                $value = ' ';
            }
        }

        return ['pattern' => $pattern, 'replace' => $replace];
    }



    /**
     * removes <b>open pixel</b> / <b>tracking pixel</b> img elements from html <b>e.g.:</b>
     * - img src="https://www.pw-footprints.de/9W7EckqTqQ/1.gif?l=308&t=7306"
     * - img src="https://ad.doubleclick.net/ddm/trackimp/N773418.3144264PERFORMANCEWERK_D/B27183803.327380928;dc_trk_aid=519325832;dc_trk_cid=97604511;ord=[timestamp];dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=;gdpr=${GDPR};gdpr_consent=${GDPR_CONSENT_755};ltd=?"
     * @param string $html
     * @return string
     */
    private function removeOpenPixelImages(string $html):string
    {
        $domElementsToRemove = [];
        // start the DOM parser
        $doc = new \DOMDocument();
        // load HTML content
        @$doc->loadHTML($html);
        // search for all img tags
        $imageElements = $doc->getElementsByTagName('img');

        // 1st step: collect all dom elements to remove
        foreach ($imageElements as $imageElement) {
            /**
             * @var \DOMNode $imageElement
             */
            $originalUrl = $imageElement->getAttribute('src');

            $this->logger->info('$originalUrl', [$originalUrl, __METHOD__, __LINE__]);

            $fileExtension = '';
            $isImage = $this->isImage($originalUrl, $fileExtension);

            if(!$isImage) { // you cannot remove here => just collect the dom elements (open pixel img's)
                $this->logger->info('will remove img with src', [$originalUrl, __METHOD__, __LINE__]);
                $domElementsToRemove[] = $imageElement;
            }
        }

        // 2nd step: remove all dom elements / open pixels to remove
        foreach ($domElementsToRemove as $imgToRemove) {
            /**
             * @var \DOMNode $imgToRemove
             */
            $imgToRemove->parentNode->removeChild($imgToRemove);
        }

        return $doc->saveHTML();
    }


    /**
     * @param string $url
     * @param string $fileExtension => keep the extension found
     * @return bool
     */
    private function isImage(string $url, string &$fileExtension):bool
    {
        $bool = false;

        $mimeArray = explode('.', $url);
        $fileExtension = $mimeArray[count($mimeArray)-1];

        if(in_array(strtolower($fileExtension), ['png', 'gif', 'jpg', 'jpeg','tiff', 'webp', 'bmp', 'svg'])) {
            $bool = true;
        }

        return $bool;
    }


    /**
     * @param string $html
     * @return string
     */
    public function inlineCss(string $html):string
    {
        $cssToInlineStyles = new CssToInlineStyles();
        $inlineCssHtml = $cssToInlineStyles->convert($html);

        if( $_ENV['APP_ENV'] === 'dev') {
            file_put_contents(__DIR__.DIRECTORY_SEPARATOR.'../../var/log/html_inlinecss.hmtl', $inlineCssHtml);
        }

        return urldecode($inlineCssHtml);
    }
}
